<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    // One-to-one Relationship
    public function contact()
    {
        return $this->hasOne(Contact::class, "iconId_FK", "iconId");
    }
}

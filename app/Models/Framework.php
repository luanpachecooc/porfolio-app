<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Framework extends Model
{
    use HasFactory;

    //One-to-one Relationship
    public function framework()
    {
        return $this->hasOne(Framework::class, "iconId_FK", "iconId");
    }
}

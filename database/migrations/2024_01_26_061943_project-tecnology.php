<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use League\CommonMark\Reference\Reference;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create("project-tecnology", function (Blueprint $table) {
            $table->id('project-tecnologyId');

            $table->json('tecnologies');

            $table->unsignedBigInteger('projectId_FK');
            $table->foreign('projectId_FK')
                ->references('projectId')
                ->on('project');

            $table->unsignedBigInteger('tecnologyId_FK');
            $table->foreign('tecnologyId_FK')
                ->references('tecnologyId')
                ->on('tecnology');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('profile', function(Blueprint $table){
            $table->id('profileId');
            $table->string('firstname', 200);
            $table->string('lastname', 200);
            $table->date('birth');
            $table->string('birthplace', 500);
            $table->string('jobTitle', 500);
            $table->string('especialization', 500);
            $table->longText('about');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};

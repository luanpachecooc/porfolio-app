<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('framework', function (Blueprint $table) {
            $table->id('frameworkId');

            $table->string('name', 200)->unique();
            $table->longText('description');
            $table->longText('experience');

            $table->unsignedBigInteger('iconId_FK')->nullable();
            $table->foreign('iconId_FK')
                ->references('iconId')
                ->on('icon')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};

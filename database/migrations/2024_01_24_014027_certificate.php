<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('certificate', function (Blueprint $table) {
            $table->id('certificateId');

            $table->string('name', 200);
            $table->string('issuingEntity', 500);
            $table->date('issueDate');
            $table->date('dueDate');
            $table->text('idNumber');
            $table->string('file', 500);
            $table->string('verificationLink', 2000);

            $table->unsignedBigInteger('profileId_FK')->nullable();
            $table->foreign('profileId_FK')
                ->references('profileId')
                ->on('profile')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};

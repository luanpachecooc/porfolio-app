<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('project', function (Blueprint $table) {
            $table->id('projectId');

            $table->string('name', 500)->unique();
            $table->longText('description');
            $table->string('repository', 10000)->unique();

            $table->unsignedBigInteger('profileId_FK');
            $table->foreign('profileId_FK')
                ->references('profileId')
                ->on('profile')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('skillId_FK')->nullable();
            $table->foreign('skillId_FK')
                ->references('skillId')
                ->on('skill')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('tecnologyId_FK')->nullable();
            $table->foreign('tecnologyId_FK')
                ->references('tecnologyId')
                ->on('tecnology')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};

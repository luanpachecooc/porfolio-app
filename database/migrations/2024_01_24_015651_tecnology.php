<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tecnology', function (Blueprint $table) {
            $table->id('tecnologyId');
            
            $table->string('name', 200);
            $table->longText('experience');
            $table->string('framework');

            $table->unsignedBigInteger('skillId_FK')->nullable();
            $table->foreign('skillId_FK')
                ->references('skillId')
                ->on('skill')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('frameworkId_FK')->nullable();
            $table->foreign('frameworkId_FK')
                ->references('frameworkId')
                ->on('framework')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('iconId_FK')->nullable();
            $table->foreign('iconId_FK')
                ->references('iconId')
                ->on('icon')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};

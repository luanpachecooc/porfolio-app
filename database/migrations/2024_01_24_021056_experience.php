<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('experience', function (Blueprint $table) {
            $table->id('experienceId');

            $table->string('employer', 500);
            $table->string('appointment', 500);
            $table->string('place', 500);
            $table->date('startDate');
            $table->date('endDate');
            $table->longText('responsibilities');

            $table->unsignedBigInteger('profileId_FK');
            $table->foreign('profileId_FK')
                ->references('profileId')
                ->on('profile')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('skillId_FK');
            $table->foreign('skillId_FK')
                ->references('skillId')
                ->on('skill')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};

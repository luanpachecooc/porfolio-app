<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contact', function (Blueprint $table) {
            $table->id('contactId');

            $table->string('type', 200);
            $table->string('phone', 10)->unique();
            $table->string('email', 200)->unique();

            $table->unsignedBigInteger('profileId_FK');
            $table->foreign('profileId_FK')
                ->references('profileId')
                ->on('profile')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('iconId_FK')->nullable();
            $table->foreign('iconId_FK')
                ->references('iconId')
                ->on('icon')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};

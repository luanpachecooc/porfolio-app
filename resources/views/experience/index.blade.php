@extends('layouts.rootLayout')

@section('title', 'Experience')

@section('content')
    <div class="flex flex-col transition-all delay-80 duration-500 justify-center items-center opacity-0 py-2"
        id="experience">
        <h1 class="text-white text-7xl font-title ">
            My experience.
        </h1>
        <div>
            
        </div>
    </div>
    <script src="{{ asset('js/load.js') }}"></script>
@endsection

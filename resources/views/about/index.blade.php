@extends('layouts.rootLayout')

@section('title', 'About')

@section('content')
    <div class="h-[100vh-9rem] flex flex-col transition-all delay-80 duration-500 justify-center items-center opacity-0 py-2"
        id="about">
        <h1 class="text-white text-7xl font-title">
            Hey there.
        </h1>
        <div class="flex gap-x-7 pt-12">
            <img src={{ asset('img/engineer.png') }} alt="engineer" width="340">
            <p class="font-description text-white text-2xl">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris rutrum commodo consectetur. Etiam eleifend
                magna tincidunt nisi elementum, et feugiat diam gravida. In sit amet sollicitudin urna. Donec vehicula
                libero nec interdum ornare. Morbi posuere mi ut velit ullamcorper blandit. Phasellus laoreet nisl id
                tincidunt accumsan.
                <br><br>
                Phasellus aliquet, tortor sit amet sagittis varius, diam ipsum vulputate arcu, a finibus neque metus et
                lectus.
            </p>
        </div>
        <p class="font-description text-white py-5 text-2xl">
            Duis sed purus placerat, dictum dolor in, tincidunt ligula. Aenean nisi dolor, porta nec nunc id, mattis
            imperdiet elit. Quisque ut nunc turpis.
        </p>
    </div>
    <script src="{{ asset('js/load.js') }}"></script>
@endsection

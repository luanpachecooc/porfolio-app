@extends('layouts.rootLayout')

@section('title', 'Projects')

@section('content')
    <div class="flex flex-col transition-all delay-80 duration-500 justify-center items-center opacity-0 py-2"
        id="projects">
        <h1 class="text-white text-7xl font-title">My Projects.</h1>
    </div>
    <script src="{{ asset('js/load.js') }}"></script>
@endsection

@extends('layouts.rootLayout')

@section('title', 'Home')
@section('content')
    <div class="px-60 py-60 opacity-0 transition-all delay-500 duration-500" id="welcome">
        <h1 class="font-engineer text-2xl text-[#FF5277]">
            Hello, my name is Luis and i’m a
            <span class="auto-type text-purple-600 font">Programmer</span>
        </h1>
        <h1 class="font-title text-white text-7xl py-9">I make websites.</h1>
        <h1 class="text-white font-description text-3xl w-8/12">
            I am an engineer specializing in programming and
            visual design of web pages.
        </h1>
    </div>

    <div class="px-20 py-5 pt-56 r">
        <h1 class="font-title text-white text-3xl">Features Projects</h1>
        <div class="py-16 grid grid-cols-[repeat(auto-fill,minmax(200px,1fr))] gap-8">
            <x-projectCard />
            <x-projectCard />
        </div>
    </div>
    <script src="https://unpkg.com/typed.js@2.1.0/dist/typed.umd.js"></script>
    <script src="{{ asset('js/load.js') }}"></script>
    <script>
        var typed = new Typed(".auto-type", {
            strings: ["Programmer", "Developer", "BackEnd", "FrontEnd", "DBA", "FullStack", "Engineer"],
            typeSpeed: 50,
            backSpeed: 70,
            loop: true
        });
    </script>
@endsection

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite('resources/css/app.css')
    <title>@yield('title')</title>
</head>
<body class="bg-[#0E141B] flex content-center justify-center">
    <x-navbar />
    <main class="pt-36">
        @yield('content')
    </main>
</body>

</html>

<header class="flex justify-between absolute w-full px-10">
    <a href="/" class="py-6 hover:opacity-50 hover:py-10 transition-all">
        <img src="{{ asset('img/Logo - Claro.png') }}" width="85" alt="Luis Ochoa">
    </a>
    <span class="flex lg:hidden hover:scale-[1.20] transition-all cursor-pointer" id="open-menu-button">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="-5 -7 24 24" width="50" fill="white">
            <path
                d="M1 0h5a1 1 0 1 1 0 2H1a1 1 0 1 1 0-2zm7 8h5a1 1 0 0 1 0 2H8a1 1 0 1 1 0-2zM1 4h12a1 1 0 0 1 0 2H1a1 1 0 1 1 0-2z">
            </path>
        </svg>
    </span>
    <nav class="main-menu hidden lg:flex lg:py-14 text-white text-lg font-navbar" id="main-menu">
        <span class="flex justify-end lg:hidden p-10 hover:scale-[1.02] transition-all cursor-pointer" id="close-menu-button">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-6 -6 24 24" width="50" fill="white">
                <path
                    d="M7.314 5.9l3.535-3.536A1 1 0 1 0 9.435.95L5.899 4.485 2.364.95A1 1 0 1 0 .95 2.364l3.535 3.535L.95 9.435a1 1 0 1 0 1.414 1.414l3.535-3.535 3.536 3.535a1 1 0 1 0 1.414-1.414L7.314 5.899z">
                </path>
            </svg>
        </span>
        <ul class="flex max-lg:flex-col max-lg:w-full max-lg:h-full max-lg:justify-center max-lg:items-center max-lg:gap-y-10 lg:gap-x-8">
            <li class="hover:py-2 hover:opacity-50 transition-all"> <a href="/about"> About me </a> </li>
            <li class="hover:py-2 hover:opacity-50 transition-all"> <a href="/projects"> Projects </a>
            </li>
            <li class="hover:py-2 hover:opacity-50 transition-all"> <a href=""> Notes </a> </li>
            <li class="hover:py-2 hover:opacity-50 transition-all"> <a href="/experience"> Experience
                </a> </li>
        </ul>
    </nav>
    <script src={{ asset('/js/menu.js') }}></script>
</header>

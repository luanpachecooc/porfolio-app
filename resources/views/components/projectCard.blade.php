<div
    class="card bg-slate-800 h-[300px] relative rounded-lg hover:bg-black hover:scale-[1.06] transition-all overflow-hidden">
    <h1 class="title absolute text-white text-3xl p-4 w-full font-bold font-description z-10">GasPrice app </h1>
    <h3 class="desc absolute bottom-0 w-full px-4 transition-all text-white text-md font-bold font-description z-10"
        >Aplicacion web que informa sobre el precio de la gasolina de las estaciones locales.</h3>
    <img src={{ asset('/img/gas-station.jpg') }} alt="" class="w-full h-full object-cover transition-all">
</div>

window.addEventListener('load', function() {
    var front = document.getElementById('welcome');
    if(front){
        front.style.opacity = '1';
        front.style.padding = '2.5rem 5rem';
    }
});

window.addEventListener('load', function() {
    var about = document.getElementById('about');
    if(about){
        about.style.opacity = '1';
        about.style.padding = '0 5rem';
    }
});

window.addEventListener('load', function() {
    var exp = document.getElementById('experience');
    if(exp){
        exp.style.opacity = '1';
        exp.style.padding = '0 5rem';
    }
});

window.addEventListener('load', function() {
    var pro = document.getElementById('projects');
    if(pro){
        pro.style.opacity = '1';
        pro.style.padding = '0 5rem';
    }
});